/**
 * @file	imprime.h
 * @brief	Arquivo com funções que realizam impressões dos dados no terminal.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	30/03/2017
 * @date	03/03/2017
 */
 
 #include <iostream>
 using std:: cout;
 using std:: endl;

 #include "operacoes.h"

 #ifndef IMPRIME_H_
 #define IMPRIME_H_

/**
 * @brief Função que imprime os elementos do vetor.
 * @param *v Ponteiro que recebe o endereço do vetor.
 * @param tamanho Recebe o tamanho do vetor.
 */
 template <typename tipo>
 void imprime_vetor (tipo *v, int tamanho) {
     if (tamanho == 0) {
         cout << "Vetor: [ ]";
     }
     else {
        cout << "Vetor: [";
        for (int ii = 0; ii < tamanho; ii++) {
            if (ii+1 == tamanho) {
                cout << v[ii] << "]" << endl;
            }
            else {
                cout << v[ii] << ", ";
            }
        }
     cout << endl << endl;
     }

 }

 /**
 * @brief Função que imprime o resultado de uma operação.
 * @param escolha Variavel que recebe o tipo de operação escolhida pelo usuário.
 * @param resultado Variavel que recebe o resultado de uma operação.
 * @param valor Variavel que recebe o numero/string específo para a comparação, nas operaçẽos.
 */
 template <typename tipo1>
 void imprime_resultado (op:: tipo_operacao escolha, tipo1 resultado, tipo1 valor ) {
    
    switch (escolha) {
        
        case op::opMax:
            cout << "Maior valor no vetor: " << resultado << endl << endl << endl;
        break;
        
        case op::opMin:
            cout << "Menor valor no vetor: " << resultado << endl << endl << endl;
        break;

        case op::opSum:
            cout << "Soma dos elementos do vetor: " << resultado << endl << endl << endl;
        break;

        case op::opAvg:
            cout << "Média dos elementos do vetor: " << resultado << endl << endl << endl;
        break;

        case op::opHig:
			cout << "Quantidade de elementos maiores que " << valor << ": " << resultado << endl << endl << endl;
        break;

        case op::opLow:
			cout << "Quantidade de elementos menores que " << valor << ": " << resultado << endl << endl << endl;
        break;
    }
 }

 template <> /** < Especialização da função imprime_resultado para o tipo string */
 void imprime_resultado <string> (op:: tipo_operacao escolha, string resultado, string valor ) {
    
    switch (escolha) {
        
        case op::opMax:
            cout << "String de maior tamanho no vetor: " << resultado << endl << endl << endl;
        break;
        
        case op::opMin:
            cout << "String de menor tamanho no vetor: " << resultado << endl << endl << endl;
        break;

        case op::opSum:
            cout << "Concatenação de todas as strings contidas no vetor: " << resultado << endl << endl << endl;
        break;

        case op::opAvg:
            cout << "Concatenação das strings cujo tamanho seja igual à média dos tamanhos de todas";
            cout << " as strings contidas no vetor: " << resultado << endl << endl << endl;
        break;

        case op::opHig:
			cout << "Concatenação das strings cujo tamanho é maior que " << valor << ": " << resultado << endl << endl << endl;
        break;

        case op::opLow:
			cout << "Concatenação das strings cujo tamanho é menor que  " << valor << ": " << resultado << endl << endl << endl;
        break;
    }
 }
 #endif