/**
 * @file	preenche.h
 * @brief	Arquivo com as funções para preencher o vetor.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	30/03/2017
 * @date	03/03/2017
 */
 
 #include <iostream>
 using std:: cout;
 using std:: cin;
 using std:: endl;

 #include <string>
 using std:: string;

 #include <cstdlib>

 #ifndef PREENCHE_H_
 #define PREENCHE_H_

/**
 * @brief Função que preenche o vetor com elementos aleatórios.
 * @param *v Ponteiro que recebe o endereço do vetor.
 * @param tamanho Recebe o tamanho do vetor.
 */
 template <typename tipo>
 void preencher (tipo *v, int tamanho) {
    srand( (unsigned)time(NULL) ); // semente para o rand.
    for (int ii = 0; ii < tamanho; ii++) { 
    v[ii] = 1 + ( (rand () % (tamanho) ) + (rand() % (1000) + 1)/1000.f );
    }
 }

 template <> /** < Especialização para o tipo inteiro */
 void preencher (int *v, int tamanho) {
    srand( (unsigned)time(NULL) ); // semente para o rand.
    for (int ii = 0; ii < tamanho; ii++) { 
	v[ii] = 1 + (rand () % tamanho );
    }
 }

template <> /** < Especialização para o tipo string */
void preencher <string> (string *v, int tamanho) {
    cout << "Digite o vetor de strings (Apenas com espaços, sem virgulas!): ";
    for (int ii = 0; ii < tamanho; ii++) { 
	cin >> v[ii];
    }
 }

#endif