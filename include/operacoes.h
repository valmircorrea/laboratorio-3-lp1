/**
 * @file	operacoes.h
 * @brief	Arquivo com as funções de operações.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	30/03/2017
 * @date	03/03/2017
 */

#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <string>
using std::string;
using std::stringstream;

#ifndef OPERACOES_H_
#define OPERACOES_H_

namespace op { // criando um namespace para as operaçẽos.
    typedef enum  {

        opMax = 1,      /**< Maior valor presente no vetor */
        opMin,          /**< Menor valor presente no vetor */
        opSum,          /**< Soma de todos os valores presentes no vetor */
        opAvg,          /**< Valor médio (parte inteira) de todos os valores presentes no vetor */
        opHig,          /**< Quantidade de elementos maiores que um determinado valor */
        opLow           /**< Quantidade de elementos menores que um determinado valor */
    } tipo_operacao;
}

template <typename tipo1>
tipo1 (*ponteiroFuncao) (tipo1 *vector, int t, tipo1 v); /** < ponteiro para funções de operaçẽos. */

/**
 * @brief Função que encontra o maior valor do vetor.
 * @param *v Recebe o vetor de inteiros.
 * @param tamanho Tamanho do vetor.
 * @param def Recebe o valor já definido na chamada dessa função.
 * @return Retorna o maior valor presente no vetor.
 */
 template <typename tipo1>
 tipo1 operacaoMax (tipo1 *v, int tamanho, tipo1 def) {

     tipo1 maior = v[0];
     for (int ii = 1; ii < tamanho; ii++) {
         if (maior < v[ii]) {
             maior = v[ii];
         }
     }
    return maior; // retorna o maior valor
 }

 template <> /** < Especializando o template da funcao operacaoMax para strings */
 string operacaoMax <string> ( string *v,  int tamanho,  string def) {

     string maior = v[0];
     for (int ii = 1; ii < tamanho; ii++) {
         if (maior.size() < v[ii].size()) {
             maior = v[ii];
         }
     }
    return maior; // retorna o maior valor
 }

/**
 * @brief Função que encontra o menor valor do vetor.
 * @param *v Recebe o vetor de inteiros.
 * @param tamanho Tamanho do vetor.
 * @param def Recebe o valor já definido na chamada dessa função.
 * @return Retorna o menor valor presente no vetor.
 */
 template <typename tipo1> 
 tipo1 operacaoMin (tipo1 *v, int tamanho, tipo1 def) {

         tipo1 menor = v[0];
         for (int jj = 1; jj < tamanho; jj++){
             if (menor > v[jj]) {
                 menor = v[jj];
             }
         }
    return menor;
 }

 template <> /** < Especializando o template da funcao operacaoMin para strings */
 string operacaoMin < string> ( string *v,  int tamanho, string def) {

    string menor = v[0];
    for (int jj = 1; jj < tamanho; jj++){
        if (menor.size() > v[jj].size()) {
            menor = v[jj];
        }
    }
    return menor;
 }


/**
 * @brief Função que soma de todos os valores presentes no vetor.
 * @param *v Recebe o vetor de inteiros.
 * @param tamanho Tamanho do vetor.
 * @param def Recebe o valor já definido na chamada dessa função.
 * @return Retorna a soma dos valores do vetor.
 */
 template <typename tipo1>
 tipo1 operacaoSum (tipo1 *v, int tamanho, tipo1 def) {

     tipo1 sum = 0;
     for (int ii = 0; ii < tamanho; ii++){
        sum += v[ii];
     }

     return sum;
 }

 template <> /** < Especializando o template da funcao operacaoSum para strings.*/
 string operacaoSum < string > ( string *v,  int tamanho,  string def) {

     string sum;
     for (int ii = 0; ii < tamanho; ii++){
        sum += v[ii];
     }
     return sum;
 }

 /**
 * @brief Função que calcula o valor médio (parte inteira) de todos os valores presentes no vetor.
 * @param *v Recebe o vetor de inteiros.
 * @param tamanho Tamanho do vetor.
 * @param def Recebe o valor já definido na chamada dessa função.
 * @return Retorna a soma dos valores do vetor.
 */
 template <typename tipo1>
 tipo1 operacaoAvg (tipo1 *v, int tamanho, tipo1 def) {

    tipo1 sum = 0;
    for (int ii = 0; ii < tamanho; ii++) {
        sum += v[ii];
     }

     return sum/tamanho;
 }

 template <>  /** < Especializando o template da funcao operacaoAvg para strings.*/
 string  operacaoAvg < string > ( string *v,  int tamanho,  string def) {
     
    unsigned int sum = 0;
    string word;
    for (int ii = 0; ii < tamanho; ii++) {
        sum += v[ii].size();
     }

     sum /=tamanho;
    
     for (int ii = 0; ii < tamanho; ii++) {
        if ( sum == v[ii].size() ) {
            word += v[ii];
        }
     }
     return word;
 }

 /**
 * @brief Função que calcula a quantidade de elementos maiores que um determinado valor.
 * @param *v Recebe o vetor de inteiros.
 * @param tamanho Tamanho do vetor.
 * @param num Valor de base para verificar os elementos maiores que ele.
 * @return Retorna a quantidade de elementos maiores que um determinado valor.
 */
 template <typename tipo1> 
 tipo1 operacaoHig (tipo1 *v, int tamanho, tipo1 num) {
         
    int cont = 0;
    for (int jj = 0; jj < tamanho; jj++){
        if (num < v[jj]) {
            cont++;
        }
    }

    return cont;
 }

 template <>  /** < Especializando o template da funcao operacaoHigpara strings*/
 string  operacaoHig < string > ( string *v,  int tamanho,  string num) {
         
    string word;
    unsigned int comp;
    comp = atoi(num.c_str());
    for (int jj = 0; jj < tamanho; jj++){
        if ( comp < v[jj].size() ) {
            word += v[jj];
        }
    }
     return word;
 }

  /**
 * @brief Função que calcula a quantidade de elementos menores que um determinado valor.
 * @param *v Recebe o vetor de inteiros.
 * @param tamanho Tamanho do vetor.
 * @param num Valor de base para verificar os elementos menores que ele.
 * @return Retorna a quantidade de elementos menores que um determinado valor.
 */
 template <typename tipo1> 
 tipo1 operacaoLow (tipo1 *v, int tamanho, tipo1 num) {
         
    int cont = 0;
    for (int jj = 0; jj < tamanho; jj++){
        if (num > v[jj]) {
            cont++;
        }
    }

    return cont;
 }

 template <> /** < Especializando o template da funcao operacaoLow para strings.*/
 string operacaoLow < string > ( string *v,  int tamanho,  string num) {
         
    string word;
    unsigned int comp;
    comp = atoi(num.c_str());
    for (int jj = 0; jj < tamanho; jj++){
        if (comp > v[jj].size()) {
            word += v[jj];
        }
    }
    return word;
 }

#endif