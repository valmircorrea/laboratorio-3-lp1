/**
 * @file	menu.h
 * @brief	Arquivo com função de menu.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	30/03/2017
 * @date	03/03/2017
 */

 #ifndef MENU_H_
 #define MENU_H_

 /**
 * @brief Função que gera um menu interativo.
 * @return Retorna a operação escolhida elo usuário.
 */
 int menu ();

 #endif