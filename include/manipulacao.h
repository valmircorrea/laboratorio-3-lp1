/**
 * @file	manipulacao.h
 * @brief	Arquivo com função que selecionar o tipo de operação a ser realizada.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	30/03/2017
 * @date	03/03/2017
 */

#include <iostream>
using std:: cout;
using std:: cin;

#include <string>
using std::string;


#include "operacoes.h"
#include "imprime.h"

#ifndef MANIPULACAO_H_
#define MANIPULACAO_H_

/**
 * @brief Função que seleciona qual o tipo de operação que o ponteiroFuncao tem que apontar.
 * @param *vetor Ponteiro que recebe o endereço do vetor.
 * @param tamanho Variavel que recebe o tamanho do vetor.
 * @param operacao Variavel que recebe o tipo de operação escolhida pelo usuário
 * @param valor Variavel que recebe o numero específo para a comparação, nas operaçẽos.
 */
template <typename tipo1>
void doVector (tipo1 *vetor, int tamanho, op:: tipo_operacao operacao, tipo1 valor) { // atribuir a função ao ponteiro

    switch (operacao) {
        
        case op::opMax:
            ponteiroFuncao <tipo1> = operacaoMax;
        break;
        
        case op::opMin:
            ponteiroFuncao <tipo1> = operacaoMin;
        break;

        case op::opSum:
            ponteiroFuncao <tipo1> = operacaoSum;
        break;

        case op::opAvg:
            ponteiroFuncao <tipo1> = operacaoAvg;
        break;

        case op::opHig:
            cout << "Informe o valor base: ";
            cin >> valor;
            ponteiroFuncao <tipo1> = operacaoHig;
        break;

        case op::opLow:
            cout << "Informe o valor limite: ";
            cin >> valor;
            ponteiroFuncao <tipo1> = operacaoLow;
        break;
    }

    imprime_resultado (operacao,ponteiroFuncao <tipo1>(vetor,tamanho,valor), valor); // imprimir os resultados
}

#endif