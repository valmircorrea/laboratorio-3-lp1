#Makefile for "laboratorio-02" C++ application
#Created by Valmir Correa 03/04/2017

RM = rm -rf

# Compilador:
CC = g++

# Variaveis para os subdiretorios:

INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test

# Opcoes de compilacao:
CFLAGS = -Wall -pedantic -ansi -std=c++14 -I. -I$(INC_DIR)

# Assegura que os alvos não sejam confundidos com os arquivos de mesmo nome:
.PHONY: all clean distclean doxy

# Define o alvo para a compilação completa:
all: lab03

debug: CFLAGS += -g -O0
debug: lab03

# Alvo para a contrução do executavel:
lab03: $(OBJ_DIR)/main.o $(OBJ_DIR)/menu.o 
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main.o:
$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/preenche.h $(INC_DIR)/imprime.h $(INC_DIR)/manipulacao.h $(INC_DIR)/operacoes.h $(INC_DIR)/menu.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do main.o:
$(OBJ_DIR)/menu.o: $(SRC_DIR)/menu.cpp $(INC_DIR)/menu.h
	$(CC) -c $(CFLAGS) -o $@ $< 

# Alvo para a geração automatica de documentacao usando o Doxygen:
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile
doxyg:
	doxygen -g

# Alvo usado para limpar os arquivos temporarios (objeto):
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
