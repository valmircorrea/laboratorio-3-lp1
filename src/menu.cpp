/**
 * @file	menu.cpp
 * @brief	Arquivo com função de menu.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	30/03/2017
 * @date	03/03/2017
 */

 #include <iostream>
 using std:: cout;
 using std:: cin;
 using std:: endl;

 #include "menu.h"

 /**
 * @brief Função que gera um menu interativo.
 * @return Retorna a operação escolhida elo usuário.
 */
 int menu () {

    int operacao;

	cout << "Operações:" << endl;
	cout << "(1) Maior valor" << endl;
	cout << "(2) Menor valor" << endl;
	cout << "(3) Soma dos elementos" << endl;
	cout << "(4) Media dos elementos" << endl;
	cout << "(5) Elementos maiores que um valor" << endl;
	cout << "(6) Elementos menores que um valor" << endl;
	cout << "(0) Sair" << endl;
	cout << "Digite sua opção: ";
	cin >> operacao;              // Opcao de operação a ser escolhida pelo usuario

    cout << endl << endl;

    return operacao;
 }