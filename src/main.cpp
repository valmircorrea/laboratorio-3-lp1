/**
 * @file	main.cpp
 * @brief	Arquivo principal do programa que realiza operacoes sobre um vetor de diferentes tipos de dados.
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	28/03/2017
 * @date	30/03/2017
 */

 #include <iostream>
 using std:: cout;
 using std:: cin;
 using std:: endl;

 #include <string>
 using std::string;

#include "preenche.h"
#include "imprime.h"
#include "manipulacao.h"
#include "operacoes.h"
#include "menu.h"

/** @brief Funcao principal que realiza operações sobre um vetor*/
int main() {
	
	string tipo_dados;				// Tipo de dados (int, float, double,string)
	cout << "Tipo de dados: ";
	cin >> tipo_dados;

	int tamanho;
	cout << "Informe a quantidade de elementos: ";
	cin >> tamanho;

	// Possiveis vetores a serem alocados dinamicamente conforme a de diferentes tipos de dados necessidade:
	int* vint = NULL;			// Vetor de inteiros
	float* vfloat = NULL;		// Vetor de decimais (float)
	double* vdouble = NULL;		// Vetor de decimais (double)
	string* vstring = NULL;		// Vetor de strings

		// Alocacao, preenchimento e impressao do vetor de acordo com o tipo.
		if (tipo_dados == "int") {
			vint = new int [tamanho];         // Alocar dinamicamente o vetor de inteiros
			preencher <int> (vint, tamanho);     // Preencher o vetor chamando funcao genérica
			imprime_vetor <int> (vint,tamanho);        // Imprimir o vetor chamando funcao genérica
		}
		else if (tipo_dados == "float") {
			vfloat = new float [tamanho];     // Alocar dinamicamente o vetor de floats
			preencher <float> (vfloat, tamanho);   
			imprime_vetor <float> (vfloat,tamanho);       
		}
		else if (tipo_dados == "double") {
			vdouble = new double [tamanho];   // Alocar dinamicamente o vetor de doubles.
			preencher <double> (vdouble, tamanho);    
			imprime_vetor <double> (vdouble,tamanho);       
		}
		else if (tipo_dados == "string") {
			vstring = new string [tamanho];   // Alocar dinamicamente o vetor de strings.
			preencher <string> (vstring, tamanho);
			imprime_vetor <string> (vstring,tamanho); 
		}
		else {
			cout << "Tipo de dados especificado incorretamente! Reinicie o programa e tente novamente." << endl;
			return 0;
		}

	int operacao;
	do {
		
		operacao = menu (); // Apresentar opções de operações ao usuario.

		if (operacao != 0) {
			
			if ( operacao >= 1 && operacao <=6) {
				
				if (tipo_dados == "int") {
					if (tamanho == 0) {
						cout << "Não é possível realizar essa operação com um vetor vazio!" << endl;
						return 0;
					}
					else {
						doVector (vint, tamanho, op::tipo_operacao(operacao), 0);
					}
				}
				else if (tipo_dados == "float") {
					if (tamanho == 0) {
						cout << "Não é possível realizar essa operação com um vetor vazio!" << endl;
						return 0;
					}
					else {
						float aux = 0;
						doVector (vfloat, tamanho, op::tipo_operacao(operacao), aux);
					}
				} 
				else if (tipo_dados == "double") {
					if (tamanho == 0) {
						cout << "Não é possível realizar essa operação com um vetor vazio!" << endl;
						return 0;
					}
					else {
						double aux2 = 0;
						doVector (vdouble, tamanho, op::tipo_operacao(operacao), aux2);
					}
				} 
				else {
					if (tamanho != 0) {
						string aux3 = " ";
						doVector (vstring, tamanho, op::tipo_operacao(operacao), aux3);
					}
					else {
						cout << "Não é possível realizar essa operação com um vetor vazio!" << endl;
						return 0;
					}
				}
			}
			else {
				cout << "Opção de operação inválida!" << endl << endl;
			}
		}
	} while (operacao != 0);

	cout << "Programa encerrado." << endl;
	return 0;
}